package abarca.alexander.cl.logic;

import abarca.alexander.cl.entities.administrador.Administrador;
import abarca.alexander.cl.entities.administrador.AdministradorDAO;
import abarca.alexander.cl.entities.servicio.Servicio;
import abarca.alexander.cl.entities.servicio.ServicioDAO;

/**
 * @author Alexander Abarca
 * Clase que se encarga de gestionar los servicios de un administrador
 * en la base de datos
 */
public class GestorAdmin {
    AdministradorDAO adminDAO = new AdministradorDAO();

    public boolean isAdmin() {
        boolean result = false;
        try {
            return adminDAO.isAdministrador();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public void registrarAdmin(String _usuario, String _clave, String _nombre, String _primerApellido, String _segundoApellido, String _identificacion , String _direccion) {
        Administrador admin = new Administrador(_usuario, _clave, _nombre, _primerApellido, _segundoApellido, _identificacion, _direccion);
        try {
            adminDAO.registrarAdmin(admin);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean sesionAdmin(String _correo, String _clave){
        boolean result = false;
        try {
            result = adminDAO.sesionAdmin(_correo, _clave);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }


}


