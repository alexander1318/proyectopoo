package abarca.alexander.cl.logic;

import abarca.alexander.cl.entities.administrador.Administrador;
import abarca.alexander.cl.entities.conductor.Conductor;
import abarca.alexander.cl.entities.transporte.Transporte;
import abarca.alexander.cl.entities.usuario.Usuario;

/**
 * @author Alexander Abarca
 * @version 1.0
 *
 * Clase que se encarga de manejar todo lo relacionado con los usuarios
 */
public class Gestor {
    private Administrador administrador;
    private Usuario usuario;
    private Conductor conductor;
    private Transporte transporte;
}
