package abarca.alexander.cl.logic;

import abarca.alexander.cl.entities.servicio.Servicio;
import abarca.alexander.cl.entities.servicio.ServicioDAO;

import java.util.ArrayList;

/**
 * GestorServicio
 * Clase que se encarga de manejar los servicios
 * @author Alexander Abarca
 */
public class GestorServicio {
    ServicioDAO servicioDAO = new ServicioDAO();

    public void registrarServicio(String descripcion, double precio, String tipo){
        Servicio newServicio = new Servicio(descripcion, precio, tipo);
        newServicio.registar((Object) newServicio);
    }
    public ArrayList<Object> listarServicios(){
        ArrayList<Object> listaServicios = new ArrayList<>();
        Servicio new_servicio = new Servicio();
        return listaServicios = new_servicio.listar();
    }
    public boolean existeId(int _id){
        boolean existe = false;
        try {
            existe = servicioDAO.existeId(_id);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return existe;
    }
    public void inactivarServicio(int _id){
        try {
            servicioDAO.inactivarServicio(_id);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
