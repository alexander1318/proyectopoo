package abarca.alexander.cl.entities.administrador;
import java.sql.*;

/**
 * @author Alexander Abarca
 * @version 1.0
 *
 * Clase que permite la conexion con la base de datos y realiza las operaciones
 * necesarias para la entidad Administrador.
 */
public class AdministradorDAO {

    public boolean isAdministrador() {
      boolean result = false;
      try {
          Class.forName("com.mysql.cj.jdbc.Driver");
          Connection conn = null;
          String query = "SELECT * FROM administrador";
          Statement stmt = null;
          ResultSet rs = null;
          String strConexion = "jdbc:mysql://localhost:3306/proyecto?autoReconnect=true&useSSL=false";
          String UserName = "root";
          String Password = "root";
          conn = DriverManager.getConnection(strConexion, UserName, Password);
          stmt = conn.createStatement();
          rs = stmt.executeQuery(query);// el rs almacena la información de la base de datos.
          if (rs.next()) {result = true;}

          conn.close();
      }
      catch (ClassNotFoundException e){
          System.out.println(e.getMessage());
      }
      catch (SQLException e){
          System.out.println(e.getMessage());
      }
      return result;
    }
    public void registrarAdmin(Administrador _admin) {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection conn = null;
            String query = "INSERT INTO administrador(usuario, clave, nombre, apellido, apellido2, identificacion, direccion) VALUES (?,?,?,?,?,?,?)";
            PreparedStatement stmt = null;
            String strConexion = "jdbc:mysql://localhost:3306/proyecto?autoReconnect=true&useSSL=false";
            String UserName = "root";
            String Password = "root";
            conn = DriverManager.getConnection(strConexion, UserName, Password);
            stmt = conn.prepareStatement(query);
            stmt.setString(1, _admin.getUsuario());
            stmt.setString(2, _admin.getClave());
            stmt.setString(3, _admin.getNombre());
            stmt.setString(4, _admin.getApellido());
            stmt.setString(5, _admin.getApellido2());
            stmt.setString(6, _admin.getIdentificacion());
            stmt.setString(7, _admin.getDireccion());
            stmt.execute();
            conn.close();
        }
        catch (ClassNotFoundException e){
            System.out.println(e.getMessage());
        }
        catch (SQLException e){
            System.out.println(e.getMessage());
        }
    }

    public boolean sesionAdmin(String _correo, String _clave) {
        boolean result = false;
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection conn = null;
            String query = "SELECT * FROM administrador WHERE usuario = ? AND clave = ?";
            PreparedStatement stmt = null;
            ResultSet rs = null;
            String strConexion = "jdbc:mysql://localhost:3306/proyecto?autoReconnect=true&useSSL=false";
            String UserName = "root";
            String Password = "root";
            conn = DriverManager.getConnection(strConexion, UserName, Password);
            stmt = conn.prepareStatement(query);
            stmt.setString(1, _correo);
            stmt.setString(2, _clave);
            rs = stmt.executeQuery();
            if (rs.next()) {result = true;}
            conn.close();
        }
        catch (ClassNotFoundException e){
            System.out.println(e.getMessage());
        }
        catch (SQLException e){
            System.out.println(e.getMessage());
        }
        return result;
    }

}

