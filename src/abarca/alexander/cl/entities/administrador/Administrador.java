package abarca.alexander.cl.entities.administrador;
import abarca.alexander.cl.entities.persona.Persona;

/**
 * @author Alexander Abarca
 * @version 1.0
 *  Clase que representa a un administrador
 *  Hereda de la clase Persona
 */
public class Administrador extends Persona {
    public Administrador() {
        super();
    }

    /**
     * @param nombre Nombre del administrador
     * @param usuario Usuario del administrador
     * @param clave Clave del administrador
     * @param nombre Usuario del administrador
     * @param apellido Apellido del administrador
     * @param apellido2 Apellido 2 del administrador
     * @param identificacion Identificacion del administrador
     * @param direccion Direccion del administrador
     */
    public Administrador(String usuario, String clave, String nombre, String apellido, String apellido2, String identificacion, String direccion) {
        super(usuario, clave, nombre, apellido, apellido2, identificacion, direccion);
    }
    @Override
    public String toString() {
        return super.toString();
    }
}
