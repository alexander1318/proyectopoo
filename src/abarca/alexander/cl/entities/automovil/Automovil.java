package abarca.alexander.cl.entities.automovil;
import abarca.alexander.cl.entities.transporte.Transporte;
import java.util.ArrayList;

/**
 * @author Alexander Abarca
 * @version 1.0
 * Clase que representa un automovil
 */
public class Automovil extends Transporte {
    private ArrayList<String> tipoAuto;
    private String color;
    private byte anno;

    private Automovil(){
        super();
    }


    public ArrayList<String> getTipoAuto() {
        return tipoAuto;
    }

    public void setTipoAuto(ArrayList<String> tipoAuto) {
        this.tipoAuto = tipoAuto;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public byte getAnno() {
        return anno;
    }

    public void setAnno(byte anno) {
        this.anno = anno;
    }

    @Override
    public String toString() {
        return "Automovil{" +
                "tipoAuto=" + tipoAuto +
                ", color='" + color + '\'' +
                ", anno=" + anno +
                '}';
    }
}
