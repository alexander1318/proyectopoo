package abarca.alexander.cl.entities.conductor;

import abarca.alexander.cl.entities.pedido.Pedido;
import abarca.alexander.cl.entities.persona.Persona;
import abarca.alexander.cl.interfaces.ICatalogos;
import abarca.alexander.cl.interfaces.IDatos;
import java.util.ArrayList;
import java.util.Date;

/**
 * @author Alexander Abarca
 * @version 1.0
 * Clase que representa a un conductor
 */
public class Conductor extends Persona implements ICatalogos, IDatos {
    private Object avatar;
    private Date fechaNacimiento;
    private int edad;
    private ArrayList<Pedido> pedidos;

    public Conductor() {
        super();
        this.avatar = null;
        this.fechaNacimiento = null;
        this.edad = 0;
        this.pedidos = new ArrayList<Pedido>();
    }

    public Object getAvatar() {
        return avatar;
    }

    public void setAvatar(Object avatar) {
        this.avatar = avatar;
    }



    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public ArrayList<Pedido> getPedidos() {
        return pedidos;
    }

    public void setPedidos(ArrayList<Pedido> pedidos) {
        this.pedidos = pedidos;
    }

    @Override
    public String toString() {
        return "Conductor{" +
                "avatar=" + avatar +
                ", fechaNacimiento=" + fechaNacimiento +
                ", edad=" + edad +
                ", pedidos=" + pedidos +
                '}';
    }

    public ArrayList<Pedido> pedidosPendientes(ArrayList<Pedido> pedidos) {
        ArrayList<Pedido> pedidosPendientes = new ArrayList<Pedido>();
        return pedidosPendientes;
    }

    public void cambiarEstadoPedidos(ArrayList<Pedido> pedidos) {}

    public void cargarGasolina(double cantidad) {}

    public double calculoTotalPedidos(ArrayList<Pedido> pedidos) {
        double total = 0;
        return total;
    }

    public void pedidosAsignados(ArrayList<Pedido> pedidos) {}
    public void pedidosAtendidos(ArrayList<Pedido> pedidos) {}

    @Override
    public void registar(Object obj) {

    }

    @Override
    public ArrayList<Object> listar() {
        return null;
    }

    @Override
    public void inactivar(int id) {

    }
}




