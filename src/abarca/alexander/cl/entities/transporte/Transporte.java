package abarca.alexander.cl.entities.transporte;

/**
 * @author Alexander Abarca
 * @version 1.0
 *
 * Clase que representa un transporte.
 */
public class Transporte {
    private String tipoTransporte;
    private String placa;
    private String marca;

    public Transporte(){
        this.tipoTransporte = "";
        this.placa = "";
        this.marca = "";
    }

    public Transporte(String tipoTransporte, String placa, String marca){
        this.tipoTransporte = tipoTransporte;
        this.placa = placa;
        this.marca = marca;
    }

    public String getTipoTransporte() {
        return tipoTransporte;
    }

    public void setTipoTransporte(String tipoTransporte) {
        this.tipoTransporte = tipoTransporte;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    @Override
    public String toString() {
        return "Transporte{" +
                "tipoTransporte='" + tipoTransporte + '\'' +
                ", placa='" + placa + '\'' +
                ", marca='" + marca + '\'' +
                '}';
    }
}
