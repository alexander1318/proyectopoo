package abarca.alexander.cl.entities.motocicleta;
import abarca.alexander.cl.entities.transporte.Transporte;
import java.util.ArrayList;

/**
 * @author Alexander Abarca
 * @version 1.0
 * Clase que representa una motocicleta
 */
public class Motocicleta extends Transporte {
    private ArrayList<String> tipoMoto;

    public Motocicleta(){
        super();
        tipoMoto = new ArrayList<>();
    }


    public void setTipoMoto(ArrayList<String> tipoMoto){
        this.tipoMoto = tipoMoto;
    }

    public ArrayList<String> getTipoMoto(){
        return tipoMoto;
    }

    @Override
    public String toString() {
        return "Motocicleta{" +
                "tipoMoto=" + tipoMoto +
                '}';
    }
}
