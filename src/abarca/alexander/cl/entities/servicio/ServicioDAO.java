package abarca.alexander.cl.entities.servicio;

import java.sql.*;
import java.util.ArrayList;

/**
 * @author Alexander Abarca
 * @version 1.0
 *
 * Clase que se encarga de realizar las operaciones de la base de datos
 * para el objeto Servicio.
 */
public class ServicioDAO {
    public void registrarServicio(Servicio _servicio) {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection conn = null;
            String query = "INSERT INTO servicio(descripcion, precio_Base, tipo_Servicio, estado_Servicio, precio_Litro) VALUES (?,?,?,?,?)";
            PreparedStatement stmt = null;
            String strConexion = "jdbc:mysql://localhost:3306/proyecto?autoReconnect=true&useSSL=false";
            String UserName = "root";
            String Password = "root";
            conn = DriverManager.getConnection(strConexion, UserName, Password);
            stmt = conn.prepareStatement(query);
            stmt.setString(1, _servicio.getDescripcion());
            stmt.setDouble(2, _servicio.getPrecioBase());
            stmt.setString(3, _servicio.getTipoServicio());
            stmt.setBoolean(4, _servicio.isEstadoServicio());
            stmt.setDouble(5, _servicio.getPrecioLitro());
            stmt.execute();
            conn.close();
        } catch (ClassNotFoundException e) {
            System.out.println(e.getMessage());
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    public ArrayList<Object> listarServicios(){
      ArrayList<Object> listaServicios = new ArrayList<>();
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection conn = null;
            String query = "SELECT * FROM servicio";
            PreparedStatement stmt = null;
            ResultSet rs = null;
            String strConexion = "jdbc:mysql://localhost:3306/proyecto?autoReconnect=true&useSSL=false";
            String UserName = "root";
            String Password = "root";
            conn = DriverManager.getConnection(strConexion, UserName, Password);
            stmt = conn.prepareStatement(query);
            rs = stmt.executeQuery(query);// el rs almacena la información de la base de datos.
            // TENGO QUE RECORRERLA
            while (rs.next()) { //rs.next devuelve true si hay más líneas en el result set. por defecto, al iniciar el ciclo, el rs está en la línea 0.
                //System.out.println(rs.getString("NOMBRE") + " " + rs.getString("IDENTIFICACION"));
                Servicio servicio = new Servicio();
                servicio.setId(rs.getInt("id"));
                servicio.setDescripcion(rs.getString("descripcion"));
                servicio.setPrecioBase(rs.getDouble("precio_Base"));
                servicio.setTipoServicio(rs.getString("tipo_Servicio"));
                servicio.setEstadoServicio(rs.getBoolean("estado_Servicio"));
                servicio.setPrecioLitro(rs.getDouble("precio_Litro"));
                listaServicios.add(servicio);
            }
            conn.close();
        }
        catch (ClassNotFoundException e){
            System.out.println(e.getMessage());
        }
        catch (SQLException e){
            System.out.println(e.getMessage());
        }
        return listaServicios;
    }
    public boolean existeId(int _id){
        boolean existe = false;
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection conn = null;
            String query = "SELECT * FROM servicio WHERE id = ?";
            PreparedStatement stmt = null;
            ResultSet rs = null;
            String strConexion = "jdbc:mysql://localhost:3306/proyecto?autoReconnect=true&useSSL=false";
            String UserName = "root";
            String Password = "root";
            conn = DriverManager.getConnection(strConexion, UserName, Password);
            stmt = conn.prepareStatement(query);
            stmt.setInt(1, obtenerId());
            rs = stmt.executeQuery();
            if(rs.next()){
                existe = true;
            }
            conn.close();
        }
        catch (ClassNotFoundException e){
            System.out.println(e.getMessage());
        }
        catch (SQLException e){
            System.out.println(e.getMessage());
        }
        return existe;
    }
    public void inactivarServicio(int _id) {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection conn = null;
            String query = "UPDATE servicio SET estado_Servicio = ? WHERE id = ?";
            PreparedStatement stmt = null;
            String strConexion = "jdbc:mysql://localhost:3306/proyecto?autoReconnect=true&useSSL=false";
            String UserName = "root";
            String Password = "root";
            conn = DriverManager.getConnection(strConexion, UserName, Password);
            stmt = conn.prepareStatement(query);
            stmt.setBoolean(1, false);
            stmt.setInt(2, obtenerId());
            stmt.execute();
            conn.close();
        } catch (ClassNotFoundException e) {
            System.out.println(e.getMessage());
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    public int obtenerId(){
        int id = 0;
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection conn = null;
            String query = "SELECT id FROM servicio ORDER BY id DESC LIMIT 1";
            PreparedStatement stmt = null;
            ResultSet rs = null;
            String strConexion = "jdbc:mysql://localhost:3306/proyecto?autoReconnect=true&useSSL=false";
            String UserName = "root";
            String Password = "root";
            conn = DriverManager.getConnection(strConexion, UserName, Password);
            stmt = conn.prepareStatement(query);
            rs = stmt.executeQuery();
            if(rs.next()){
                id = rs.getInt("id");
            }
            conn.close();
        }
        catch (ClassNotFoundException e){
            System.out.println(e.getMessage());
        }
        catch (SQLException e){
            System.out.println(e.getMessage());
        }
        return id;
    }

}

