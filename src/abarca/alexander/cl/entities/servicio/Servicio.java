package abarca.alexander.cl.entities.servicio;
import abarca.alexander.cl.interfaces.ICatalogos;
import java.util.ArrayList;

/**
 * @author Alexander Abarca
 * @version 1.0
 *
 * Clase que representa un servicio
 */
public class Servicio implements ICatalogos {
    private int id;
    private String descripcion;
    private double precioBase;
    private String tipoServicio;
    private boolean estadoServicio;
    private double precioLitro;
    ServicioDAO servicioDAO = new ServicioDAO();

    public Servicio(){
        this.id = 0;
        this.descripcion = "null";
        this.precioBase = 0;
        this.tipoServicio = "null";
        this.estadoServicio = false;
        this.precioLitro = 0;
    }

    public Servicio(String descripcion, double precioBase, String tipoServicio, boolean estadoServicio, double precioLitro){
        this.id = 0;
        this.descripcion = descripcion;
        this.precioBase = precioBase;
        this.tipoServicio = tipoServicio;
        this.estadoServicio = estadoServicio;
        this.precioLitro = precioLitro;
    }
    public Servicio(String descripcion, double precio, String tipoServicio){
        this.id=0;
        this.descripcion = descripcion;
        this.precioBase = precio;
        this.tipoServicio = tipoServicio;
        this.estadoServicio = true;
        this.precioLitro = 0;

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public double getPrecioBase() {
        return precioBase;
    }

    public void setPrecioBase(double precioBase) {
        this.precioBase = precioBase;
    }

    public String getTipoServicio() {
        return tipoServicio;
    }

    public void setTipoServicio(String tipoServivio) {
        this.tipoServicio = tipoServicio;
    }

    public boolean isEstadoServicio() {
        return estadoServicio;
    }

    public void setEstadoServicio(boolean estadoServicio) {
        this.estadoServicio = estadoServicio;
    }

    public double getPrecioLitro() {
        return precioLitro;
    }

    public void setPrecioLitro(double precioLitro) {
        this.precioLitro = precioLitro;
    }

    @Override
    public String toString() {
        return "Servicio{" +
                "id=" + id + '\'' +
                "descripcion='" + descripcion + '\'' +
                ", precioBase=" + precioBase +
                ", tipoServicio='" + tipoServicio + '\'' +
                ", estadoServicio=" + estadoServicio +
                ", precioLitro=" + precioLitro +
                '}';
    }

    @Override
    public void registar(Object _servicio) {
        try {
            servicioDAO.registrarServicio((Servicio) _servicio);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public ArrayList<Object> listar() {
        ArrayList<Object> listaServicios = new ArrayList<>();
        try {
            listaServicios = servicioDAO.listarServicios();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listaServicios;
    }

    @Override
    public void inactivar(int _id) {
        try {
            servicioDAO.inactivarServicio(_id);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
