package abarca.alexander.cl.entities.direccion;
import java.util.ArrayList;

/**
 * @author Alexander Abarca
 * @version 1.0
 * Clase que representa una direccion
 *
 */
public class Direccion {
    private int codDireccion;
    private ArrayList<String> provincia;
    private ArrayList<String> canton;
    private ArrayList<String> distrito;

    public Direccion(){
        provincia = new ArrayList<>();
        canton = new ArrayList<>();
        distrito = new ArrayList<>();
    }

    public void setCodDireccion(int codDireccion){
        this.codDireccion = codDireccion;
    }

    public int getCodDireccion(){
        return codDireccion;
    }

    public void setProvincia(ArrayList<String> provincia){
        this.provincia = provincia;
    }

    public ArrayList<String> getProvincia(){
        return provincia;
    }

    public void setCanton(ArrayList<String> canton){
        this.canton = canton;
    }

    public ArrayList<String> getCanton(){
        return canton;
    }

    public void setDistrito(ArrayList<String> distrito){
        this.distrito = distrito;
    }

    public ArrayList<String> getDistrito(){
        return distrito;
    }

    public String toString(){
        String direccion = "";
        direccion += "Codigo de direccion: " + codDireccion + "\n";
        direccion += "Provincia: " + provincia + "\n";
        direccion += "Canton: " + canton + "\n";
        direccion += "Distrito: " + distrito + "\n";
        return direccion;
    }
}
