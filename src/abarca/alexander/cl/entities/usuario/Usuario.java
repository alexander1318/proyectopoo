package abarca.alexander.cl.entities.usuario;

import abarca.alexander.cl.entities.metodoPago.MetodoPago;
import abarca.alexander.cl.entities.pedido.Pedido;
import abarca.alexander.cl.entities.persona.Persona;
import abarca.alexander.cl.entities.transporte.Transporte;
import abarca.alexander.cl.interfaces.ICatalogos;
import abarca.alexander.cl.interfaces.IDatos;

import java.util.ArrayList;
import java.util.Date;

/**
 * @author Alexander Abarca
 * @version 1.0
 *
 * Clase que representa a un usuario
 */
public class Usuario extends Persona implements ICatalogos, IDatos {
    private Object avatar;
    private Date fechaNacimiento;
    private int edad;
    private char genero;
    private String celular;
    private Transporte transporte;
    private MetodoPago metodoPago;
    private byte codVerificacion;

    public Usuario() {
        super();
    }

    public Usuario(Object avatar, Date fechaNacimiento, int edad, char genero, String celular, Transporte transporte, MetodoPago metodoPago, byte codVerificacion) {
        super();
        this.avatar = avatar;
        this.fechaNacimiento = fechaNacimiento;
        this.edad = edad;
        this.genero = genero;
        this.celular = celular;
        this.transporte = transporte;
        this.metodoPago = metodoPago;
        this.codVerificacion = codVerificacion;
    }

    public Object getAvatar() {
        return avatar;
    }

    public void setAvatar(Object avatar) {
        this.avatar = avatar;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public char getGenero() {
        return genero;
    }

    public void setGenero(char genero) {
        this.genero = genero;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public Transporte getTransporte() {
        return transporte;
    }

    public void setTransporte(Transporte transporte) {
        this.transporte = transporte;
    }

    public MetodoPago getMetodoPago() {
        return metodoPago;
    }

    public void setMetodoPago(MetodoPago metodoPago) {
        this.metodoPago = metodoPago;
    }

    public byte getCodVerificacion() {
        return codVerificacion;
    }

    public void setCodVerificacion(byte codVerificacion) {
        this.codVerificacion = codVerificacion;
    }

    @Override
    public String toString() {
        return "Usuario{" +
                "avatar=" + avatar +
                ", fechaNacimiento=" + fechaNacimiento +
                ", edad=" + edad +
                ", genero=" + genero +
                ", celular='" + celular + '\'' +
                ", transporte=" + transporte +
                ", metodoPago=" + metodoPago +
                ", codVerificacion=" + codVerificacion +
                '}';
    }

    public void verPedidos(Pedido pedidos, Transporte transporte) {
    }

    @Override
    public void registar(Object obj) {

    }

    @Override
    public ArrayList<Object> listar() {
        return null;
    }

    @Override
    public void inactivar(int id) {

    }
}
