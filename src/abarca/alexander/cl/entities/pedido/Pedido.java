package abarca.alexander.cl.entities.pedido;

import abarca.alexander.cl.entities.transporte.Transporte;
import abarca.alexander.cl.entities.usuario.Usuario;
import abarca.alexander.cl.entities.direccion.Direccion;
import abarca.alexander.cl.entities.metodoPago.MetodoPago;
import java.util.ArrayList;
import java.util.Date;

/**
 * @author Alexander
 * @version 1.0.0
 *
 * Clase que representa un pedido
 */
public class Pedido {
    private ArrayList<Pedido> pedidos;
    private ArrayList<Transporte> transportes;
    private int numPedido;
    private Date fechaCreacionPedido;
    private Usuario datosCliente;
    private Direccion direccionPedido;
    private MetodoPago pagoPedido;

    public Pedido(){
        pedidos = new ArrayList<>();
        transportes = new ArrayList<>();
    }

    public ArrayList<Pedido> getPedidos() {
        return pedidos;
    }

    public void setPedidos(ArrayList<Pedido> pedidos) {
        this.pedidos = pedidos;
    }

    public ArrayList<Transporte> getTransportes() {
        return transportes;
    }

    public void setTransportes(ArrayList<Transporte> transportes) {
        this.transportes = transportes;
    }

    public int getNumPedido() {
        return numPedido;
    }

    public void setNumPedido(int numPedido) {
        this.numPedido = numPedido;
    }

    public Date getFechaCreacionPedido() {
        return fechaCreacionPedido;
    }

    public void setFechaCreacionPedido(Date fechaCreacionPedido) {
        this.fechaCreacionPedido = fechaCreacionPedido;
    }

    public Usuario getDatosCliente() {
        return datosCliente;
    }

    public void setDatosCliente(Usuario datosCliente) {
        this.datosCliente = datosCliente;
    }

    public Direccion getDireccionPedido() {
        return direccionPedido;
    }

    public void setDireccionPedido(Direccion direccionPedido) {
        this.direccionPedido = direccionPedido;
    }

    public MetodoPago getPagoPedido() {
        return pagoPedido;
    }

    public void setPagoPedido(MetodoPago pagoPedido) {
        this.pagoPedido = pagoPedido;
    }

    @Override
    public String toString() {
        return "Pedido{" +
                "pedidos=" + pedidos +
                ", transportes=" + transportes +
                ", numPedido=" + numPedido +
                ", fechaCreacionPedido=" + fechaCreacionPedido +
                ", datosCliente=" + datosCliente +
                ", direccionPedido=" + direccionPedido +
                ", pagoPedido=" + pagoPedido +
                '}';
    }


}
