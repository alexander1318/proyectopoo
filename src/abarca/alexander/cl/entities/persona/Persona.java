package abarca.alexander.cl.entities.persona;

import abarca.alexander.cl.entities.direccion.Direccion;

/**
 * @author Alexander Abarca
 * @version 1.0
 *
 * Clase Persona

 */
public abstract class Persona {
    private String nombre;
    private String apellido;
    private String apellido2;
    private String identificacion;
    private String usuario;
    private String clave;
    private String direccion;

    public Persona(){
        nombre = "null";
        apellido = "null";
        apellido2 = "null";
        identificacion = "null";
        usuario = "null";
        clave = "null";
    }
    public Persona(String usuario, String clave, String nombre, String apellido, String apellido2, String identificacion, String direccion){
        this.nombre = nombre;
        this.apellido = apellido;
        this.apellido2 = apellido2;
        this.identificacion = identificacion;
        this.usuario = usuario;
        this.clave = clave;
        this.direccion = direccion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getApellido2() {
        return apellido2;
    }

    public void setApellido2(String apellido2) {
        this.apellido2 = apellido2;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String toString(){
        return "Nombre: " + nombre + "\nApellido: " + apellido + "\nApellido2: " + apellido2 + "\nIdentificacion: " + identificacion + "\nUsuario: " + usuario + "\nClave: " + clave + "\nDireccion: " + direccion;
    }
}
