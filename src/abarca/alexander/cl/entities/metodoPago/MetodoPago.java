package abarca.alexander.cl.entities.metodoPago;
import java.util.ArrayList;
import java.util.Date;

/**
 * @author Alexander Abarca
 * @version 1.0
 *
 * Clase que representa un metodo de pago
 */
public class MetodoPago {
    private int numTarjeta;
    private ArrayList<String> proveedor;
    private Date fechaVencimiento;
    private byte cvv;

    public MetodoPago() {
        this.numTarjeta = 0;
        this.proveedor = new ArrayList<>();
        this.fechaVencimiento = new Date();
        this.cvv = 0;
    }

    public MetodoPago(int numTarjeta, ArrayList<String> proveedor, Date fechaVencimiento, byte cvv) {
        this.numTarjeta = numTarjeta;
        this.proveedor = proveedor;
        this.fechaVencimiento = fechaVencimiento;
        this.cvv = cvv;
    }

    public int getNumTarjeta() {
        return this.numTarjeta;
    }

    public void setNumTarjeta(int numTarjeta) {
        this.numTarjeta = numTarjeta;
    }

    public ArrayList<String> getProveedor() {
        return this.proveedor;
    }

    public void setProveedor(ArrayList<String> proveedor) {
        this.proveedor = proveedor;
    }

    public Date getFechaVencimiento() {
        return this.fechaVencimiento;
    }

    public void setFechaVencimiento(Date fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public byte getCvv() {
        return this.cvv;
    }

    public void setCvv(byte cvv) {
        this.cvv = cvv;
    }

    public String toString() {
        return "Numero de tarjeta: " + this.numTarjeta + "\n" +
                "Proveedor: " + this.proveedor + "\n" +
                "Fecha de vencimiento: " + this.fechaVencimiento + "\n" +
                "Codigo de seguridad: " + this.cvv;
    }
}
