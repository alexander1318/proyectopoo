package abarca.alexander.cl.interfaces;

import java.util.Date;

/**
 * Interfaz que define los metodos que debe implementar una clase que
 * implemente esta interfaz.
 * @author Alexander Abarca
 */
public interface IDatos {
    public Object avatar = null;
    public Date fechaNacimiento = null;
    public int edad=0;

    public void setAvatar(Object avatar);
    public void setFechaNacimiento(Date fechaNacimiento);
    public void setEdad(int edad);

    public Object getAvatar();
    public Date getFechaNacimiento();
    public int getEdad();
}


