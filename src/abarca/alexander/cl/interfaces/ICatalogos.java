package abarca.alexander.cl.interfaces;

import abarca.alexander.cl.entities.servicio.Servicio;
/**
 * Interfaz que define los metodos que debe implementar el catalogo de servicios
 * @author Alexander Abarca
 */
import java.util.ArrayList;

public interface ICatalogos {
    public void registar(Object obj);
    public ArrayList<Object> listar();
    public void inactivar(int id);

}
